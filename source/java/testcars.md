# Test Cars

<table style="width:100%">
<tr><td align="center"><img src="../img/testcars.png" alt="image"></td></tr><tr>
<td>Description:<br>
Passes argument into function to show the make, model, mpg, and year made of certain cars
</td></tr>

<tr><td>Download:<br>
<li><a href="https://mega.nz/#!lN10hRqD!uNk1-FGKe1mCdP6XkDwgsZOgB8rzccspJDRgYN_EdvY">binary</a></li>
<li><a href="https://mega.nz/#!tNl0kKQK!ckVf3bYteSdNAa7gZraxykO1HrBxVFuiE_4b4MkdZ2w">source</a></li>
</td>

</tr></table>
