# Random Number Game

<table style="width:100%">
<tr><td align="center"><img src="../img/testrandomnumbergame.png" alt="image"></td></tr><tr>
<td>Description:<br>
computer picks a number 1-100 and the user guesses and the computer reveals the number it picked.
</td></tr>

<tr><td>Download:<br>
<li><a href="https://mega.nz/#!pQsikCya!m3PIzCMUQ6XzAnA7WjoLza79Lt2EcRTvTMpcKy9Yeok">binary</a></li>
<li><a href="https://mega.nz/#!dZkEmIxQ!xFf6FCdegcnQeTHPnvOKVDNOsy6_0BlK8CXcfNffkY8">source</a></li>
</td>

</tr></table>
