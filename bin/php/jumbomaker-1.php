<!DOCTYPE html PUBLIC "-//W#C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns ="http://www.w3.org/1999/xhtml">
<head><title> PHP Code Blocks </title><meta http-equiv="content-type" content="text/html; charset=iso-8859-1" /></head><body>

<?php
function displayError($fieldName, $errorMsg){
global $errorCount;
echo "Error for \"$fieldName\": $errorMsg<br>";
++$errorCount;
}

function validateWord($data,$fieldname){
global $errorCount;
if (empty($data)){
	displayError($fieldname,"This field is required!");
	$retval="";
}
	else { 	$retval=trim($data);
			$retval= stripslashes($retval);
			if ((strlen($retval)<4) || (strlen($retval)>7))
				displayError($fieldname, "Words must be at least 4 and 7 characters long");
															
			
			if (preg_match("/^[a-z]+$/i",$retval))
				displayError($fieldname, "Characters must be A-Z or a-z");
													
				
												 
			
		}
		
		$retval=strtoupper($retval);
				$retval=str_shuffle($retval);
				return  ($retval);
	}



$errorCount=0;
$words=array();
$words[]=validateWord($_POST['word1'],["Word 1"]);
$words[]=validateWord($_POST['word2'],["Word 2"]);
$words[]=validateWord($_POST['word3'],["Word 3"]);
$words[]=validateWord($_POST['word4'],["Word 4"]);

if ($errorCount>0)
echo "Please use the \"Back\" button to re-enter the data.<br />\n";
else {
$wordnum = 0;
foreach ($words as $word)
echo "Word ".++$wordnum.": $word<br>\n";
}



?>

</body></html>
