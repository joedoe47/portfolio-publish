<!DOCTYPE html PUBLIC "-//W#C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns ="http://www.w3.org/1999/xhtml">
<head><title> PHP Code Blocks </title><meta http-equiv="content-type" content="text/html; charset=iso-8859-1" /></head><body><?php
$password=$_GET["password"]; //?password= test, test12_ , test123_

$reg = array(
array('/.{8,16}/', "8-16 characters", "0"),
array('/[0-9]{1,}/', "1 digit", "0"),
array('/[a-z]{1,}/', "1 lower case", "0"),
array('/[A-Z]{1,}/', "1 upper case", "0"),
array('/[^0-9A-Za-z]{1,}/', "1 special characters", "0"),
array('/[^ ]/', "no spaces", "0")
);

$stop = count($reg); //we use the number of coloums to figure out where to stop

echo "checking the password you submitted: " .$password. "<hr><br><br>";
for ($i = 0; $i < $stop; $i++){
$index=$i+1;
echo "check " .$index. " - ";
    	if (preg_match($reg[$i][0],$password)){
               echo "success, password has at least " .$reg[$i][1]. "<br>";
    
    } else {   echo "fail, password does not have at least " .$reg[$i][1]. "<br>";
	           $reg[$i][2]="1"; //this can be used with another for loop to record total no. of errors
	}                          }
?></body></html>
