<?php
/*
Jose Mendoza
required programming assignment
*/

function displayError($fieldName, $errorMsg){
global $errorCount;
echo "Error for \"$fieldName\": $errorMsg<br>";
++$errorCount;
}

function validateWord($data,$fieldname){
global $errorCount;
if (empty($data)){ displayError($fieldname,"This field is required!"); $retval=""; }
else { $retval=trim($data); $retval= stripslashes($retval); // clean the data

if ((strlen($retval)>8)){ displayError($fieldname, "password can not be more than 8 characters");}
if (preg_match("/[^\w]/",$retval)){ displayError($fieldname, "invalid password!");} }
return  ($retval);
}

//main
$errorCount=0;
$words=validateWord($_POST['password'],"password");

if ($errorCount>0) echo "Please use the \"Back\" button to re-enter the data.<br />\n";
else { echo $words.", is a valid password!<br>\n"; }
?>
