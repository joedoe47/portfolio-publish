$(document).ready(function(){
var ids = ["#java","#c","#php","#bash"];
function hide(){$("input[type=button]").attr('disabled', true);$.each( ids, function(index, value){$(value).hide();});}
function show(value){$(value).fadeIn();$("input[type=button]").removeAttr('disabled');}
$("#javabutton").click(function(){hide();show(ids[0]);});
$("#cbutton").click(function(){hide();show(ids[1]);});
$("#phpbutton").click(function(){hide();show(ids[2]);});
$("#bashbutton").click(function(){hide();show(ids[3]);});
/*EOF*/});
