# Calculator

<table style="width:100%">
<tr><td align="center"><img src="../img/simplecalc.png" alt="image"></td></tr><tr>
<td>Description:<br>
A simple terminal/ncurses calculator. Can do basic arithmetic of addition, subtraction, multiplication, and dividion
</td></tr>

<tr><td>Download:<br>
<li><a href="https://mega.nz/#!ZVEGVCqC!hGWQ8xNbOMShb-HBWsmE0Pk_c8XL1pMGtSytkYSRFVU">32 bit</a></li>
<li><a href="https://mega.nz/#!UZE1jCpQ!WtvR_OpPPdVN2hD6XD2CANC6hn1_MDkhrK5UqKi1sfw">64 bit</a></li>
<li><a href="https://mega.nz/#!cQcFia4K!1A8pAf0m10_hozeFouV93H0mrKbY35oWRhgRj483Kqw">source</a></li>
</td>

</tr></table>
