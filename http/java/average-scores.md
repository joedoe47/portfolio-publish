# Average Scores

<table style="width:100%">
<tr><td align="center"><img src="../img/averagescores.png" alt="image"></td></tr><tr>
<td>Description:<br> 
gets 3 grade inputs from user and shows the average of the user's exams
</td></tr>

<tr><td>Download:<br>
<li><a href="https://mega.nz/#!VdN30KAZ!DkjuM7vdSGC94ln6BTlzqxM1HzMZo7SOX1_a_S06XNw">binary</a></li>
<li><a href="https://mega.nz/#!1cN0yD7J!YIW_mm8u5HUK2b5Cbq6S6iTLjWnqrHEeHyQ9K7cLFnw">source</a></li>
</td>

</tr></table>
