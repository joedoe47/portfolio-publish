# Other Projects

There are other projects that I work on I host them on my git server, you can easily see them of my public ones here:
[https://git.joepcs.com](https://git.joepcs.com).

#### How to clone

You might notice that when you do see a project and you try to see the content of files you only see hashes and when you try to do 'git clone' that it will fail. That is because of 2 reasons.

- Certbot (aka [Lets Encrypt](https://letsencrypt.org) )
	- This allows you to use this site via https but is still a workin progress to work via command line. 
- Git annex 
	- This is similar to what Git LFS tries to accomplish. Metadata is on the git server but the data is stored elsewhere. (on box.com, owncloud, etc.)

So if you wish to get a copy of a specific project you can email me and I will send you a link to all the data.


